#+title: Contingency Provisions in Fixed-Income Securities
#+created: [2020-11-23 Mon 11:18]
#+tags: cfa-1, cfa-1-reading-50, fixed-income-security
#+startup: overview

- tags :: [[file:20201121195348-cfa_1.org][CFA 1]]

*LOS 50.f:* 
Describe contingency provisions affecting the timing and/or nature of cash flows of fixed-income securities and identify whether such provisions benefit the borrower or the lender.


* Introduction
A *contingency provision* (or *embedded options*) describes an action that can be taken if a contigency occurs . Bonds with no contigency provisions are *option-free* (or *straight*).

* Types of Options
Here are the different types of contigency provisions.

** Call Option
+ Issuer has right to redeem all/part of bond at prespecified price (call price).
+ Example
  20-yr, 6% bond issued at par on 1st June 2012, with this call schedule in the indenture
  * Bonds can be redeemed by the issuer at 102% of par after 1st June 2017.
  * Bonds can be redeemed by the issuer at 101% of par after 1st June 2020.
  * Bonds can be redeemed by the issuer at 100% of par after 1st June 2022.
  Notes:
  + This is not callable for 5 yrs - this is *call protection* and period is called *lockout period*, (or cushion/deferment period).
  + 1st June 2017 - *first call date*, with call price %102 - 2% call premium
  + 1st June 2022 - callable at par = *first par call date*
+ Normally called if market yield on bond declines - issuer can call bond and then reissue debt with lower coupon rates to reduce capital costs.
    Market yield may go down because interest rates have dropped or credit quality of bond has increased.
+ Benefits issuer - bond must have higher yield/lower price that option-free version.
+ Three styles of exercise for callable bonds:
  * American style - bonds called anytime after first call date.
  * European style - bonds only callable on prespecified call dates.
  * Bermuda style - called on specified dates after first call date, normally on coupon payment dates.
+ *Make-whole call provision* - call price is not fixed but is calculated as the present value of the remaining coupon payments.
  * This will not lower than market value of bond, so will only be called in the event of an acquisition, a merger, or when restructuring.
  * It penalizes the issuer for calling the bond.
  
** Put Options
+ Bondholder has right to sell back bond at prespecified price (normally par).
+ Exercised when fair value of bond < put price
  * because interest rates have risen
  * or credit quality of issuer has fallen
+ Valuable to bondholder so sells at higher price than option-free.
  
** Convertible Option
+ Bondholders have the option to exchange bond for a specified number of shared in firm's common stock.
+ 
