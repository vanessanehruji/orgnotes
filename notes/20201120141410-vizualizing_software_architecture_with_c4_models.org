#+title: Vizualizing Software Architecture with C4 Models
#+roam_alias: 
#+created: [2020-11-20 Fri 14:14]

Simon Brown introduces the =C4 model= in his [[https://www.youtube.com/watch?v=zcmU-OE452k][talk]] and discusses how to standardize diagrams such that it is understandable by everyone. Much like how maps can be used by everyone despite slightly slightly differing notations (it uses a key to distinguish items on a map).

[[https://structurizr.com/share/1/diagrams#containers][Here]] are his examples of different levels of views with his tool Structurizr. This is similar to zooming on a map - you have to zoom out to get a clearer picture of the design.
