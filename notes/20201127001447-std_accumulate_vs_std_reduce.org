#+title: std::accumulate vs std::reduce
#+created: [2020-11-27 Fri 00:14]


_Takeaway_:
+ ~std::accumulate~ and ~std::reduce~ are both fold operations
+ ~std::reduce~ can be parallelized with data running on different threads (but not ~std::accumulate~)
+ You must ensure that binary operation is both associative and commutative when running a parallel ~std::reduce~.

** std::accumulate
Here is an example of ~std::accumulate~.
#+begin_src C++ :results drawer :includes <vector> <numeric> <functional> <iostream>
  std::vector<int> v {1, 2, 3, 4, 5};
  auto sum = std::accumulate(v.begin(), v.end(), 0);
  auto prod = std::accumulate(v.begin(), v.end(), 1, std::multiplies<int>());

  std::cout << "Sum: " << sum
            << "\nProduct: " << prod
            << std::endl;
#+end_src

#+RESULTS:
:results:
Sum: 15
Product: 120
:end:

** std::reduce
You can do something similar with ~std::reduce~
#+begin_src C++ :results drawer :includes <vector> <numeric> <functional> <iostream>
  std::vector<int> v {1, 2, 3, 4, 5};
  auto sum = std::reduce(v.begin(), v.end(), 0);
  auto prod = std::reduce(v.begin(), v.end(), 1, std::multiplies<int>());

  std::cout << "Sum: " << sum
            << "\nProduct: " << prod
            << std::endl;
#+end_src

#+RESULTS:
:results:
:end:

** Parallel std::reduce
The only difference in ~std::accumulate~ and ~std::reduce~ is that reduce can be parallelised with an execution policy argument.
#+begin_src C++ :results drawer :includes <vector> <numeric> <functional> <iostream>
  std::vector<int> v {1, 2, 3, 4, 5};

  // Serial versions
  auto minus = std::reduce(v.begin(), v.end(), 0, std::plus<int>());
  auto minus = std::reduce(std::execution::seq, v.begin(), v.end(), 0, std::plus<int>());

  // Parallel version
  auto minus = std::reduce(std::execution::par, v.begin(), v.end(), 0, std::plus<int>());
#+end_src

** Problems with Parallel std::reduce
If the binary operator does not satisfy the axioms of associativity and commutativity, then parallelized ~std::reduce~ produce incorrect result. A simple example of this is subtraction.

In serial, a fold operation with subtraction gives a final result of 1.
#+begin_src ditaa :file resources/fred_spe.png :cmdline -r
   +----+  +----+  +----+  +----+  +----+  +----+  +----+
   | 64 |  | 32 |  | 16 |  | 8  |  | 4  |  | 2  |  | 1  |
   +----+  +----+  +----+  +----+  +----+  +----+  +----+
     |       |       |       |       |       |       |
     +-------+       |       |       |       |       |
             |       |       |       |       |       |
             +-------+       |       |       |       |
                     |       |       |       |       |
                     +-------+       |       |       |
                             |       |       |       |
                             +-------+       |       |
                                     |       |       |
                                     +-------+       |
                                             |       |
                                             +-------+
                                                     |
                                                   +----+
                                                   | 1  |
                                                   +----+
#+end_src

However,  we get a different result in a parallelized version.
#+begin_src ditaa :file resources/fred_spe.png :cmdline -r
   +----+  +----+  +----+  +----+  +----+  +----+  +----+
   | 64 |  | 32 |  | 16 |  | 8  |  | 4  |  | 2  |  | 1  |
   +----+  +----+  +----+  +----+  +----+  +----+  +----+
     |       |       |       |       |       |       |
     +-------+       +-------+       +-------+       |
        32               8               2           |
         |               |               |           |  
         +---------------+               +-----------+
                24                             1
                 |                             |
                 +-----------------------------+            
                                |
                              +----+
                              | 23 |
                              +----+
#+end_src

** References
+ See this [[https://blog.tartanllama.xyz/accumulate-vs-reduce/][blog post]] by Sy Brand.
