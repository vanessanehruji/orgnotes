* Books
:PROPERTIES:
:AUTHOR:   Vanessa Nehruji
:EMAIL:    vanessa.nehruji@gmail.com
:STARTUP:  contents
:VISIBILITY: children
:END:

This is my personal book list.

** Pragmatic Programmer
  :PROPERTIES:
  :AUTHORS:  Hunt, Andrew; Thomas, David;
  :TAGS: Computing
  :END:
  Note 29 is hard to read without images.
*** Highlights
**** On the diference between OO and functional programming - /p. 264-265/
#+begin_quote
But there’s something deeper, too. If your background is object-
oriented programming, then your reflexes demand that you
hide data, encapsulating it inside objects. These objects then
chatter back and forth, changing each other’s state. This
introduces a lot of coupling, and it is a big reason that OO 
systems can be hard to change.

In the transformational model, we turn that on its head. Instead
of little pools of data spread all over the system, think of data as
a mighty river, a flow. Data becomes a peer to functionality: a
pipeline is a sequence of code → data → code → data…. The
data is no longer tied to a particular group of functions, as it is
in a class definition. Instead it is free to represent the unfolding
progress of our application as it transforms its inputs into its
outputs. This means that we can greatly reduce coupling: a
function can be used (and reused) anywhere its parameters
match the output of some other function.
#+end_quote
*** Things I've learnt:
**** Layers to decouple program
**** Plain text files last through time
**** Use grep to search for things in file, i.e. `grep '^mod' *.rs`
**** Design by contracts - validate inputs and run only when inputs are correct (23)
**** Use interfaces and traits instead of inheritance to pass objects of different classes into one list, i.e. Car with Bicycle in Vec<dyn Box<Movable>>.  /p. 278/
#+begin_quote
Interfaces and protocols give us polymorphism without inheritance.
#+end_quote
*** Things to learn:
**** Cucumber
**** Phoenix
**** Dynamic vs Static typing
**** Early (templates, inheritance) vs Late binding (through virtual method in C++)?
**** sed functionality
** Atomistic Spin Dynamics
** Improving the Design of Existing Code
:PROPERTIES:
:AUTHORS: Fowler, Martin
:TAGS: Computing
:END:
** Software Architecture in Practice

* Papers
:PROPERTIES:
:AUTHOR:   Vanessa Nehruji
:EMAIL:    vanessa.nehruji@gmail.com
:STARTUP:  contents
:VISIBILITY: children
:END:

And this is my personal paper list.

** A layer-based method for rapid software development
*** DOI: 10.1016/j.camwa.2012.03.082
